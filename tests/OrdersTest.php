<?php


class OrdersTest extends TestCase
{

    /**
     * check response 
     *
     * @return void
     */
    public function testSaveOrder()
    {
        $data = [
            "endpoint_url"=> "api/v1/orders.json",
            "search_url"=> "https://developer.github.com/v3/#http-redirects",
            "search_word"=> "status",
            "method"=> "POST",
            "parameters"=>
                    ["order"=> 
                                [ 
                                    "order_id"=> "51275",
                                    "email"=> "test@email.com",
                                    "total_amount_net"=> "1890.00",
                                    "shipping_costs"=> "29.00",
                                    "payment_method"=> "VISA",
                                    "items"=> [
                                                    [
                                                        "name"=> "Item1",
                                                        "qnt"=> 1,
                                                        "value"=> 1100,
                                                        "category"=> "Fashion",
                                                        "subcategory"=> "Jacket",
                                                        "tags"=> [
                                                        "porsche",
                                                        "design"
                                                        ],
                                                        "collection_id"=> 12,
                                                        "order_id"=> "51275",
                                                    ],
                                                    [
                                                        "name"=> "Item2",
                                                        "qnt"=> 1,
                                                        "value"=> 790,
                                                        "category"=> "Watches",
                                                        "subcategory"=> "sport",
                                                        "tags"=>[
                                                            "watch",
                                                            "porsche",
                                                            "electronics"
                                                            ],
                                                        "collection_id"=> 7,
                                                        "order_id"=> "51275",
                                                    ]
                                                ]
                                ]


            ]
        ];

        $return = [
            'created' => true,
         ];
        $response = $this->json('POST', '/api/v1/orders',  $data)
        ->seeJson([
            'created' => true,
         ]);;

        //$this->assertEquals(200, $response->status());
    }
  

}