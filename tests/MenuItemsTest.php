<?php

// use handing discount value
use App\Handling\Orders\MenuItems;
// get collection data
use App\Models\Collection;

class MenuItemsTest extends TestCase
{

    /*
        Count word in url content
    */
    public function testReturnMenuItems()
    {
     
        $items = [ 
                    "order_id"=> "51275",
                    "email"=> "test@email.com",
                    "total_amount_net"=> "1890.00",
                    "shipping_costs"=> "29.00",
                    "payment_method"=> "VISA",
                    "items"=> [
                                    [
                                        "name"=> "Item1",
                                        "qnt"=> 1,
                                        "value"=> 1100,
                                        "category"=> "Fashion",
                                        "subcategory"=> "Jacket",
                                        "tags"=> [
                                            "porsche",
                                            "design"
                                            ],
                                        "collection_id"=> 12
                                    ],
                                    [
                                        "name"=> "Item2",
                                        "qnt"=> 1,
                                        "value"=> 790,
                                        "category"=> "Watches",
                                        "subcategory"=> "sport",
                                        "tags"=>[
                                            "watch",
                                            "porsche",
                                            "electronics"
                                            ],
                                        "collection_id"=> 7
                                    ]
                                ]
                 ];
              
        // retrive all collections
        $collections = Collection::allCollections();
        // set discount items 
        $discountvalue = '21';
        // return valus
        $jsonreturn = [ 
                        "order_id"=> "51275",
                        "email"=> "test@email.com",
                        "total_amount_net"=> "1890.00",
                        "shipping_costs"=> "29.00",
                        "payment_method"=> "VISA",
                        "items"=> [
                                        [
                                            "name"=> "Item1",
                                            "qnt"=> 1,
                                            "value"=> 1100,
                                            "category"=> "Fashion",
                                            "subcategory"=> "Jacket",
                                            "tags"=> json_encode([
                                            "porsche",
                                            "design"
                                            ]),
                                            "collection_id"=> 12,
                                            "order_id"=> "51275",
                                        ],
                                        [
                                            "name"=> "Item2",
                                            "qnt"=> 1,
                                            "value"=> 790,
                                            "category"=> "Watches",
                                            "subcategory"=> "sport",
                                            "tags"=>json_encode([
                                                "watch",
                                                "porsche",
                                                "electronics"
                                                ]),
                                            "collection_id"=> 7,
                                            "order_id"=> "51275",
                                        ]
                                    ],
                        "discount_value"=> "21",
                        "total_amount"=> 1446.5
                     ];
        $this->assertEquals(
            MenuItems::returnMenuItems($items,$collections,$discountvalue),
            $jsonreturn
        );
    }
    


}