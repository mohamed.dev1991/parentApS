<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        
        $DiscountValue = 21;
        $items = '{
            "order_id": 51275,
            "email": "test@email.com",
            "total_amount_net": "1890.00",
            "shipping_costs": "29.00",
            "payment_method": "VISA",
            "items": [
                {
                    "name": "Item1",
                    "qnt": 1,
                    "value": 1100,
                    "category": "Fashion",
                    "subcategory": "Jacket",
                    "tags": "[\"porsche\",\"design\"]",
                    "collection_id": 12,
                    "order_id": 51275
                },
                {
                    "name": "Item2",
                    "qnt": 1,
                    "value": 790,
                    "category": "Watches",
                    "subcategory": "sport",
                    "tags": "[\"watch\",\"porsche\",\"electronics\"]",
                    "collection_id": 7,
                    "order_id": 51275
                }
            ]
        }';
        $muneafterdiscount = '{
            "order_id": 51275,
            "email": "test@email.com",
            "total_amount_net": "1890.00",
            "shipping_costs": "29.00",
            "payment_method": "VISA",
            "items": [
                {
                    "name": "Item1",
                    "qnt": 1,
                    "value": 1100,
                    "category": "Fashion",
                    "subcategory": "Jacket",
                    "tags": "[\"porsche\",\"design\"]",
                    "collection_id": 12,
                    "order_id": 51275
                },
                {
                    "name": "Item2",
                    "qnt": 1,
                    "value": 790,
                    "category": "Watches",
                    "subcategory": "sport",
                    "tags": "[\"watch\",\"porsche\",\"electronics\"]",
                    "collection_id": 7,
                    "order_id": 51275
                }
            ],
            "discount_value": 21,
            "total_amount": 1446.5
        }';        
        $returnDiscountValue = \Mockery::mock(\App\Handling\Orders\DiscountValue::class)
            ->shouldReceive('returnDiscountValue')
            ->andReturn($DiscountValue)
            ->getMock();

       $returnMenuItems = \Mockery::mock(\App\Handling\Orders\MenuItems::class)
            ->shouldReceive('returnMenuItems')
            ->andReturn($items)
            ->getMock();
       $save = \Mockery::mock(\App\Handling\Orders\SaveOrder::class)
            ->shouldReceive('save')
            ->andReturn($muneafterdiscount)
            ->getMock();  
         
    }
}
