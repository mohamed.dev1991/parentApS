<?php

// use handing discount value
use App\Handling\Orders\DiscountValue;

class DiscountValueTest extends TestCase
{

    /*
        Count word in url content
    */
    public function testReturnDiscountValue()
    {
        $url = 'https://developer.github.com/v3/#http-redirects';
        $string = 'status';
        $this->assertEquals(
            DiscountValue::returnDiscountValue($url,$string),
            '21'
        );
    }
    /*
        check link url
    */
    public function testTestUrl()
    {
        $url = 'https://developer.github.com/v3/#http-redirects';
        $this->assertEquals(
            DiscountValue::testUrl($url),
            true
        );
    }


}