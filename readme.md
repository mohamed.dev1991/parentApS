##  PHP Task

<ul>
    <li>edit config env file</li>
    <li>run migrations</li>
</ul>

Send post request data type content json to url api/v1/orders
with search_url , search_word

<pre>
   {
        "endpoint_url": "api/v1/orders.json",
        "search_url": "https://developer.github.com/v3/#http-redirects",
        "search_word": "status",
        "method": "POST",
        "parameters": {
            "order": {
            "order_id": 51275,
            "email": "test@email.com",
            "total_amount_net": "1890.00",
            "shipping_costs": "29.00",
            "payment_method": "VISA",
            "items": [
                {
                "name": "Item1",
                "qnt": 1,
                "value": 1100,
                "category": "Fashion",
                "subcategory": "Jacket",
                "tags": [
                    "porsche",
                    "design"
                ],
                "collection_id": 12
                },
                {
                "name": "Item2",
                "qnt": 1,
                "value": 790,
                "category": "Watches",
                "subcategory": "sport",
                "tags": [
                    "watch",
                    "porsche",
                    "electronics"
                ],
                "collection_id": 7
                }
            ]
            }
        }
        }
</pre>  

Will return data with discount value and total amount after calculating and insert data to database
## unit testing
run command  ./vendor/bin/phpunit

<pre>
    {
         "created": true
    }
</pre>


