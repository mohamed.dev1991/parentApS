<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
       /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders_items';
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
     protected $fillable = [
            'order_id', 'name', 'qnt', 'value', 'category', 'subcategory', 'tags', 'collection_id'
     ];
}
