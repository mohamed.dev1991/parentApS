<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{

   static function allCollections()
   {
       return array('12','7','6','5');
   }
}
