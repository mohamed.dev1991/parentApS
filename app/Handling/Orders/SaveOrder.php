<?php

namespace App\Handling\Orders;

// use model Order
use App\Models\Order;
// use model Order
use App\Models\OrderItem;

// use class database
use DB;

class SaveOrder
{
   /**
     * Return Discount Value 
     * Get url , string search
     * @return discount value
     */

    static function save($data)
    {
       // insert Order data to model
       DB::transaction(function($data) use ($data) {
        // save order main data
            $insertOrder = new Order ;     
            $insertOrder->order_id = $data['order_id'];
            $insertOrder->email = $data['email'];
            $insertOrder->total_amount_net = $data['total_amount_net'];
            $insertOrder->shipping_costs = $data['shipping_costs'];
            $insertOrder->payment_method = $data['payment_method'];
            $insertOrder->discount_value = $data['discount_value'];
            $insertOrder->total_amount = $data['total_amount'];
            $insertOrder->save();
            // save order items data
            $insertItemsOrder =  OrderItem::insert($data['items']);
        });

        return TRUE;
    }

}