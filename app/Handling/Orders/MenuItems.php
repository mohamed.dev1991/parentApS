<?php

namespace App\Handling\Orders;


class MenuItems
{
   /**
     * Return return Menu Items after discount
     * Get data , collections , discountvalue
     * @return array data to insert to database
     */

    static function returnMenuItems($data,$collections,$discountvalue)
    {

        // array for append discount value of items
        $totaldiscounts = [];
        // check discount value
        if($discountvalue != 0){
            foreach($data['items'] as $key=>$item){
                // preset tags json to save in database
                $data['items'][$key]['tags'] = json_encode($item['tags']);
                $data['items'][$key]['order_id'] = $data['order_id'];
                // check collection_id in collections array
                if(in_array($item['collection_id'],$collections)){
                    $totaldiscounts[] = $item['value'] * $discountvalue / 100 ;
                }
            }
            // sum total disount
            $totaldiscountvalue = array_sum($totaldiscounts);
            // set discount value to insert to database
            $data['discount_value'] = $discountvalue ;
            // get 25 % of total
            $get25 = $data['total_amount_net'] * 25 / 100;
            // check total discount < 25% and set total amount 
            if($totaldiscountvalue <= $get25){
                $data['total_amount'] = ($data['total_amount_net'] + $data['shipping_costs']) - $get25; 
            }else{
                $data['total_amount'] = ($data['total_amount_net'] + $data['shipping_costs']) - $totaldiscountvalue;
            }
        }else{
            $data['total_amount']   =  $data['total_amount_net'] + $data['shipping_costs'] ;
            $data['discount_value'] =  $discountvalue ;
            foreach($data['items'] as $key=>$item){
                // preset tags json to save in database
                $data['items'][$key]['tags'] = json_encode($item['tags']);
                $data['items'][$key]['order_id'] = $data['order_id'];
            }
        }
        return $data ;
    

    }


}