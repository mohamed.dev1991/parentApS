<?php

namespace App\Handling\Orders;


class DiscountValue
{
   /**
     * Return Discount Value 
     * Get url , string search
     * @return discount value
     */

    static function returnDiscountValue($url,$stringsearch)
    {
        // discount value defult 
        $DiscountValue  = 0;
        // check url response 
        $checkurlstatus = self::testUrl($url);

        // check if url 
        if($checkurlstatus === true){

            // get url content
            $urlcontent  = strtolower(file_get_contents($url));
            // get total count
            $countsearch =  substr_count(e($urlcontent),$stringsearch);
            // set Discount Value
            $DiscountValue = $countsearch;

        }

        return $DiscountValue ;

    }


       /**
     * chek url status
     *
     * @return void
     */
    static function testUrl($url)
    {
        $headers = get_headers($url);
        if($headers[0] === 'HTTP/1.1 200 OK'){
            return true;
        }else{
            return false;
        }
    }

}