<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use handing discount value
use App\Handling\Orders\DiscountValue;
// use Menu Items
use App\Handling\Orders\MenuItems;
// use Save Order
use App\Handling\Orders\SaveOrder;
// get collection data
use App\Models\Collection;


class OrdersController extends Controller
{

    public function store(Request $request) 
    {
       
         // check response has value
         if($request->has('parameters')){
            // retrive all collections
            $collections = Collection::allCollections();
            // get response value
            $data = $request->json()->all();
            // get globle discount value by search work in url
            $DiscountValue = DiscountValue::returnDiscountValue($data['search_url'],$data['search_word']);
            // handle retrving data items
            $MenuItems = MenuItems::returnMenuItems($data['parameters']['order'],$collections,$DiscountValue);
            // save order class
            $SaveOrder = SaveOrder::save($MenuItems);
            if($SaveOrder){
                // return save value
                return response([
                    'created' => true,
                 ],200);
            }else{
                  // return false
                  return response([
                    'created' => false
                            ],400);
            }
            

        }

    }
}
